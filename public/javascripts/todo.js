/* eslint-disable max-len,no-invalid-this,max-statements-per-line */
let todoList = [];
let counter = [];
let counterComplete = [];
let tab = 'allTodo';
let filteredTodos = [];


$(() => {
  const ONE_ELEMENT = 1;

  let currentPage = ONE_ELEMENT;

  let renderArr = [];

  const ENTER_KEY = 13;

  const AMOUNT_TODO_ON_PAGE = 5;
  const INITIAL_ELEMENT = 0;
  const MAXIMUM_NUMBER_OF_CHARACTERS = 30;

  // Controllers
const findError = function(response){
  if (!response.ok) throw Error(response.status);
  return response;
}
const showErrorMessage = function(){
  alert("Error!Try again,please")
}
 const getTodos = function(){
   fetch("/todos")
       .then(findError)
   .then(result=>result.json())
       .then(todos => {todoList = todos;render()})
       .catch(showErrorMessage);

};
getTodos();

const postTodos = function(value){
  const value2 = JSON.stringify({value});
  fetch("/todos",{
    method:"POST",
      body: value2,
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      }
  })
      .then(findError)
      .then(result => result.json())
      .then(result => {todoList.push(result);currentPage=0})
    .then(() => render())
  .catch(showErrorMessage);

}
const updateTodos = function(_id,check,value){
  fetch(`/todos/${_id}`,{
    method:"PATCH",
    body:  JSON.stringify({check, value}),
      headers: {
        'Accept': 'application/json',
            'Content-Type': 'application/json'
    }
  })
      .then(findError)
      .then(()=>render())
      .catch(showErrorMessage);

}
const deleteTodos = function(id){
  fetch(`/todos/${id}`,{
    method:"DELETE",

    })
      .then(findError)
      .then(()=>render())
      .catch(showErrorMessage);

}
const checkAllTodos = function(check){
    $('.radio').prop('checked', !!todoList.length);
    if (todoList.length) fetch("/todos",{
      method:"PATCH",
      body: JSON.stringify({check}),
      headers:{
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      }
  })
      .then(findError)
      .then(()=>render())
      .catch(showErrorMessage);

}
const deleteCompleteTodos = function(){
  fetch("/todos",{
    method:"DELETE"
  })
      .then(findError)
      .then(()=>render())
      .catch(showErrorMessage);

}


  // Test check
  const testCheck = function() {
    const statusTodo = $('.radio')[INITIAL_ELEMENT];

    if (todoList.length !== INITIAL_ELEMENT){
      statusTodo.checked = todoList
      .every(elem => elem.check === true);
    }
    else statusTodo.checked = false;
  };

  // Pagination
  const paginationPage = function(arr) {
    const page = Math.ceil(arr.length / AMOUNT_TODO_ON_PAGE);

    if (currentPage > page || !currentPage ) currentPage = page;
    const startTodo = (currentPage - ONE_ELEMENT) * AMOUNT_TODO_ON_PAGE;
    const finishTodo = (currentPage - ONE_ELEMENT) * AMOUNT_TODO_ON_PAGE + AMOUNT_TODO_ON_PAGE;


    renderArr.length = INITIAL_ELEMENT;




    renderArr = arr.slice(startTodo, finishTodo);
    for (let ili = ONE_ELEMENT; ili <= page; ili++) {
      const pageHTML = `<li class="page-item ${currentPage === ili ? 'active' : ''}">
                        <a class="page-link" id="${ili}" href="#">${ili}</a>
                        </li>`;


      $('nav ul#pagination').append(pageHTML);
    }
  };


  // Counters
  const counterTodo = function() {
    counter = todoList.filter(elem => elem.check === false);
    counterComplete = todoList.length - counter.length;
    const stringForAppendCounter = ` <input type="button" class="todo-counter btn btn-primary" value="${counter.length} in work">
    <input type="button" class="todo-counter-completed btn btn-primary" value="${counterComplete} completed">`;


    $('#counters').empty();
    $('#counters').append(stringForAppendCounter);
  };

  function filterByMode(todo) {
    if (tab === 'activeTodo') return todo.check === false;
    else if (tab === 'completeTodo') return todo.check === true;
    else return todo;
  }


  // Render
  const render = function() {
    $('ul').empty();
    filteredTodos = todoList.filter(filterByMode);
    paginationPage(filteredTodos);
    renderArr.forEach(element => {
      if (element.check === true) {
        $('#out').append(`
            <li id="${element._id}">
              <input class="toggle input-group-text" type="checkbox" checked>
              <span class="input-group-text ${element.check}" id="inputGroup-sizing-sm">
                ${_.escape(element.value)}
              </span> 
              <input class="hide input-group-text" id="edit" type="text" value="${_.escape(element.value)}">
              <button class="delete btn btn-primary">x</button>
           </li>
        `);
      }
      else $('#out').append(`<li id="${element._id}">
                            <input class="toggle input-group-text" type="checkbox">
                            <span class="input-group-text ${element.check}" id="inputGroup-sizing-sm">${_.escape(element.value)}</span>
                            <input class="hide input-group-text" id="edit" type="text" value="${_.escape(element.value)}">
                            <button class="delete btn btn-primary">x</button> </li>`);
    });
    counterTodo();
    testCheck();
  };

  // Add
  const addTodo = function() {
      let value = $('#in').val().trim();
    if (value.length > MAXIMUM_NUMBER_OF_CHARACTERS) {
     alert('too much characters!');
     }

    if (value !== '') {
        postTodos(value);

       $('#in')[INITIAL_ELEMENT].value = '';
       $('#in')[INITIAL_ELEMENT].focus();}
     else if (value.length === INITIAL_ELEMENT) {
       alert('You cant post empty string');
    }
    filteredTodos = todoList.filter(filterByMode);
  };


  $('#add').on('click', addTodo);

  // Delete
  const deleteTodo = function() {
    const thisId = ($(this).parent()
      .prop('id'));

    $(this).parent()
      .remove();
    todoList.forEach(todo => {
      if (thisId === todo._id) todoList.splice(todoList.indexOf(todo), ONE_ELEMENT);
    });
    deleteTodos(thisId);
  };


  $('body').on('click', '.delete', deleteTodo);

  // Check
  const checkTodo = function() {
    const thisId = ($(this).parent()
      .prop('id'));
    const check = Boolean($(this).prop('checked'));
  const value = this.nextElementSibling.innerText;
    todoList.forEach(todo => {
      if (thisId == todo._id && todo.check === false) todo.check = true;
      else if (thisId == todo._id && todo.check === true) todo.check = false;
    });
      updateTodos(thisId, check,value);
    if (tab === 'completeTodo') {
      filteredTodos = todoList.filter(filterByMode);
      // currentPage = Math.ceil(filteredTodos.length / AMOUNT_TODO_ON_PAGE);
    }
  };


  $('body').on('click', '.toggle', checkTodo);

  // Check all
  const checkAllTodo = function() {
    const isAllTodosChecked = $(this)[INITIAL_ELEMENT].checked;


    todoList.forEach(todo => {
      if (isAllTodosChecked) todo.check = true;
      else todo.check = false;
    });
    checkAllTodos($(this)[INITIAL_ELEMENT].checked);
  };

  $('.radio').on('click', checkAllTodo);

  // Delete complete
  const clearCompleteTodo = function() {
    todoList = todoList.filter(elem => elem.check === false);
    todoList.forEach(function todo() {
      if (todo.check === true) todoList.splice(todo.check, ONE_ELEMENT);
    });
    deleteCompleteTodos();
  };

  $('.clear-completed').on('click', clearCompleteTodo);

  // Edit
  const editTodo = function() {
    $(this).addClass('hide');
    $(this.nextElementSibling).removeClass('hide')
      .focus();
  };

  $('#out').on('dblclick', 'span', editTodo);


  const saveEdit = function() {
    const newValue = $(this)[INITIAL_ELEMENT];
    const index = todoList.findIndex(todo => todo._id == ($(this)[INITIAL_ELEMENT].parentElement.id));
    const check = Boolean($(this).parent('li').children('input.toggle.input-group-text').prop('checked'));

    if ($(this)[INITIAL_ELEMENT].value.trim() === '') {
      $(this).addClass('hide');
      $(this.previousElementSibling).removeClass('hide');
    } else if (newValue.value.length < MAXIMUM_NUMBER_OF_CHARACTERS) {
      todoList[index].value = newValue.value.trim();
      $(this).addClass('hide');
      $(this.previousElementSibling).removeClass('hide');
    }
    else if (newValue.value.length > MAXIMUM_NUMBER_OF_CHARACTERS){
        alert('too much characters!');
        $(this).prop('value',todoList[index].value);
        $(this).addClass('hide');
        $(this.previousElementSibling).removeClass('hide');
    }
    updateTodos($(this)[INITIAL_ELEMENT].parentElement.id,check, newValue.value);
  };

  $('body').on('blur', 'li input#edit', saveEdit);

  // Tabs

  const getAllTodo = function() {
    tab = 'allTodo';
    $('button.allTodo')[INITIAL_ELEMENT].classList = 'allTodo btn btn-primary active';
    $('button.activeTodo')[INITIAL_ELEMENT].classList = 'activeTodo btn btn-primary';
    $('button.completeTodo')[INITIAL_ELEMENT].classList = 'completeTodo btn btn-primary';
    render();
  };

  $('button.allTodo').on('click', getAllTodo);
  const getActiveTodo = function() {
    tab = 'activeTodo';
    $('button.activeTodo')[INITIAL_ELEMENT].classList = 'activeTodo btn btn-primary active';
    $('button.completeTodo')[INITIAL_ELEMENT].classList = 'completeTodo btn btn-primary';
    $('button.allTodo')[INITIAL_ELEMENT].classList = 'allTodo btn btn-primary';
    render();
  };

  $('button.activeTodo').on('click', getActiveTodo);
  const getCompleteTodo = function() {
    tab = 'completeTodo';
    $('button.completeTodo')[INITIAL_ELEMENT].classList = 'completeTodo btn btn-primary active';
    $('button.allTodo')[INITIAL_ELEMENT].classList = 'allTodo btn btn-primary';
    $('button.activeTodo')[INITIAL_ELEMENT].classList = 'activeTodo btn btn-primary';
    render();
  };

  $('button.completeTodo').on('click', getCompleteTodo);
  const ChangePage = function() {
    currentPage = Number($(this)[INITIAL_ELEMENT].id);
    render();
  };


  $('ul#pagination').on('click', 'li a', ChangePage);


  // Enter

  const addEnter = function(event) {
    if (event.which === ENTER_KEY) addTodo();
  };

  const addEnterEdit = function(event) {
    if (event.which === ENTER_KEY) {
      const newValue = $(this)[INITIAL_ELEMENT];
      const index = todoList.findIndex(todo => todo._id == ($(this)[INITIAL_ELEMENT].parentElement.id));
      const check = Boolean($(this).parent('li').children('input.toggle.input-group-text').prop('checked'));

        if ($(this)[INITIAL_ELEMENT].value.trim() === '') {
            $(this).addClass('hide');
            $(this.previousElementSibling).removeClass('hide');
        } else if (newValue.value.length < MAXIMUM_NUMBER_OF_CHARACTERS) {
            todoList[index].value = newValue.value.trim();
            $(this).addClass('hide');
            $(this.previousElementSibling).removeClass('hide');
        }
        else if (newValue.value.length > MAXIMUM_NUMBER_OF_CHARACTERS){
          alert('too much characters!');
            $(this).prop('value',todoList[index].value);
                $(this).addClass('hide');
            $(this.previousElementSibling).removeClass('hide');
        }
        updateTodos($(this)[INITIAL_ELEMENT].parentElement.id,check, newValue.value);
    }

  };

  $('#in').keypress(addEnter);
  $('body').on('keydown', 'li input#edit', addEnterEdit);
});

