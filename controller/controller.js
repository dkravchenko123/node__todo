let model = require('../database/database');

const GetTodos = function(req, res){
    model.find({}).then(result => {res.send(result);});
};

const PostTodos = function(req,res) {
    console.log(req.body);
    if (req.body.value.length > 30 || req.body.value.length === 0) throw req;
    model.create(req.body)
        .then(result => {
            res.send(result)
        })
        .catch(err => {
            res.status(500);
            res.send(err);
        });
};
const UpdateTodos = function(req,res) {
    if (req.body.value.length > 30 || req.body.value.length === 0) throw req;
    model.findOneAndUpdate({_id: req.params.id}, {check: req.body.check, value: req.body.value}).then(() => {
        res.send('checked')
    })
        .catch(err => {
            res.status(500);
            res.send(err);
        });
};
const DeleteTodos = function (req,res) {
    model.deleteOne({_id: req.params.id}).then(() => {
        res.send('deleted')
            .catch(err => {
                res.status(500);
                res.send(err);
            })
    });
};
const CheckAllTodos = function(req,res){
    model.updateMany({check:req.body.check}).then(()=>{
        res.send('ok')
            .catch(err=> {
                res.status(500);
                res.send(err);
            })
    })
};
const DeleteCompleteTodos = function(req,res){
    model.deleteMany({check:true}).then(()=>{
        res.send('deleted all completed')
            .catch(err=> {
                res.status(500);
                res.send(err);
            })
    })
};

const functions = {GetTodos, PostTodos, DeleteTodos, UpdateTodos, CheckAllTodos, DeleteCompleteTodos};
module.exports = functions;
