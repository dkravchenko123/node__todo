let express = require('express');
let router = express.Router();
const {GetTodos, PostTodos, DeleteTodos, UpdateTodos, CheckAllTodos, DeleteCompleteTodos} = require('../controller/controller');

router.get('/',GetTodos);
router.post('/', PostTodos);
router.patch('/:id', UpdateTodos);
router.delete('/:id', DeleteTodos);
router.patch('/', CheckAllTodos);
router.delete('/', DeleteCompleteTodos);
module.exports = router;

